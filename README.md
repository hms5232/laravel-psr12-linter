# Laravel PSR12 Linter

為 Laravel 修改過的 PSR12 標準

參考 [laravel-fans/lint](https://github.com/laravel-fans/laravel-lint)

## 使用

1. Clone 專案下來
2. 在家目錄製作軟連結到 `phpcs.xml`，或直接複製到專案根目錄中
3. `composer global require "squizlabs/php_codesniffer=*"`
4. 在有 git repo 的專案底下執行 `{本專案路徑}/lint.sh`，或是自行製作軟連結及別名使用（例如：`alias lint='cd $(git rev-parse --show-toplevel) && laravel-psr12-lint; cd -'`）

## LICENSE

[MIT](LICENSE)
