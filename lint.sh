#!/bin/bash

phpcs --extensions=php $(git diff --diff-filter=d --name-only HEAD | grep ".php$")
